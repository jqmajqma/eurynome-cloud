package cn.herodotus.eurynome.upms.logic.repository.development;

import cn.herodotus.eurynome.data.base.repository.BaseRepository;
import cn.herodotus.eurynome.upms.api.entity.development.Supplier;

/**
 * <p> Description : SupplierRepository </p>
 *
 * @author : gengwei.zheng
 * @date : 2020/4/29 13:57
 */
public interface SupplierRepository extends BaseRepository<Supplier, String> {

}
