package cn.herodotus.eurynome.upms.logic.repository.system;

import cn.herodotus.eurynome.data.base.repository.BaseRepository;
import cn.herodotus.eurynome.upms.api.entity.system.SysRole;

public interface SysRoleRepository extends BaseRepository<SysRole, String> {

}
