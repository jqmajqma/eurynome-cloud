# postgresql 数据库创建脚本
```
CREATE USER herodotus WITH PASSWORD 'herodotus';
CREATE DATABASE herodotus OWNER herodotus;
GRANT ALL PRIVILEGES ON DATABASE herodotus TO herodotus;
```
